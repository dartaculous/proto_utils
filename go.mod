module gitlab.com/dartaculous/proto_utils

go 1.20

require (
	github.com/shopspring/decimal v1.3.1
	go.mongodb.org/mongo-driver v1.11.4
	google.golang.org/protobuf v1.30.0
)
