package proto_utils

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"google.golang.org/protobuf/types/known/durationpb"
	"google.golang.org/protobuf/types/known/timestamppb"
	"google.golang.org/protobuf/types/known/wrapperspb"
)

func MapArray[G any, E any](items []E, mapItem func(E) G) []G {
	gItems := make([]G, len(items))
	for i, item := range items {
		gItem := mapItem(item)
		gItems[i] = gItem
	}
	return gItems
}

func TimeToWrapper(value *time.Time) *timestamppb.Timestamp {
	if value == nil {
		return nil
	}
	return timestamppb.New(*value)
}

func WrapperToTime(value *timestamppb.Timestamp) *time.Time {
	if value == nil {
		return nil
	}
	t := value.AsTime()
	return &t
}

func WrapperToInt(value *wrapperspb.Int32Value) *int {
	if value == nil {
		return nil
	}
	t := int(value.Value)
	return &t
}

func WrapperToInt32(value *wrapperspb.Int32Value) *int32 {
	if value == nil {
		return nil
	}
	t := int32(value.Value)
	return &t
}

func WrapperToObjectId(value *wrapperspb.StringValue) (*primitive.ObjectID, error) {
	if value == nil {
		return nil, nil
	}
	strValue := value.Value
	if len(strValue) == 0 {
		return nil, nil
	}
	t, err := primitive.ObjectIDFromHex(strValue)
	if err != nil {
		return nil, err
	}
	return &t, nil
}

func DurationToWrapper(value *time.Duration) *durationpb.Duration {
	if value == nil {
		return nil
	}
	return durationpb.New(*value)
}

func WrapperToDuration(value *durationpb.Duration) *time.Duration {
	if value == nil {
		return nil
	}
	d := value.AsDuration()
	return &d
}

//goland:noinspection GoUnusedExportedFunction
func StringToWrapper(value *string) *wrapperspb.StringValue {
	if value == nil {
		return nil
	}
	return wrapperspb.String(*value)
}

func WrapperToString(value *wrapperspb.StringValue) *string {
	if value == nil {
		return nil
	}
	return &value.Value
}

//goland:noinspection GoUnusedExportedFunction
func Int32ToWrapper(value *int32) *wrapperspb.Int32Value {
	if value == nil {
		return nil
	}
	return wrapperspb.Int32(*value)
}

//goland:noinspection GoUnusedExportedFunction
func BoolFromWrapper(value *wrapperspb.BoolValue) *bool {
	if value == nil {
		return nil
	}
	return &value.Value
}

//goland:noinspection GoUnusedExportedFunction
func Int32FromWrapper(value *wrapperspb.Int32Value) *int32 {
	if value == nil {
		return nil
	}
	return &value.Value
}

func ToObjectId(value string) primitive.ObjectID {
	if len(value) == 0 {
		return primitive.ObjectID{}
	}
	hex, err := primitive.ObjectIDFromHex(value)
	if err != nil {
		return primitive.ObjectID{}
	}
	return hex
}
