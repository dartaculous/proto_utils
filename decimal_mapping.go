package proto_utils

import (
	"math/big"

	"github.com/shopspring/decimal"
)

func StringToRat(v string) (*big.Rat, error) {
	if len(v) == 0 {
		return nil, nil
	}
	r := &big.Rat{}
	err := r.UnmarshalText([]byte(v))
	if err != nil {
		return nil, err
	}
	return r, nil
}

func RatToString(v *big.Rat) string {
	if v == nil {
		return ""
	}

	n := decimal.NewFromBigInt(v.Num(), 0)
	d := decimal.NewFromBigInt(v.Denom(), 0)

	str := n.Div(d).String()
	return str

}

func StringToDecimal(v string) (*decimal.Decimal, error) {
	if len(v) == 0 {
		return nil, nil
	}
	d, err := decimal.NewFromString(v)
	if err != nil {
		return nil, err
	}
	return &d, nil
}

func DecimalToString(v *decimal.Decimal) (string, error) {
	if v == nil {
		return "", nil
	}
	b := v.String()
	return b, nil

}

func RatToWrapper(v *big.Rat) []byte {
	if v == nil {
		return []byte{}
	}

	numerator := v.Num()
	denominator := v.Denom()

	nBytes := bigIntToBytes(numerator)
	dBytes := bigIntToBytes(denominator)

	lBytes := encodeLength(len(nBytes))

	var bytes []byte
	bytes = append(lBytes, nBytes...)
	bytes = append(bytes, dBytes...)
	return bytes
}

func DecimalToWrapper(v *decimal.Decimal) []byte {
	if v == nil {
		return []byte{}
	}

	rat := v.Rat()

	numerator := rat.Num()
	denominator := rat.Denom()

	nBytes := bigIntToBytes(numerator)
	dBytes := bigIntToBytes(denominator)

	lBytes := encodeLength(len(nBytes))

	var bytes []byte
	bytes = append(lBytes, nBytes...)
	bytes = append(bytes, dBytes...)
	return bytes
}

func WrapperToRat(value []byte) *big.Rat {
	if len(value) == 0 {
		return nil
	}

	nLength, bytes := decodeLength(append([]byte{}, value...))
	nBytes := bytes[:nLength]
	dBytes := bytes[nLength:]

	numerator := bytesToInt64(nBytes)
	denominator := bytesToInt64(dBytes)

	r := big.NewRat(numerator, denominator)

	return r
}

func WrapperToDecimal(value []byte) *decimal.Decimal {
	if len(value) == 0 {
		return nil
	}

	nLength, bytes := decodeLength(append([]byte{}, value...))
	nBytes := bytes[:nLength]
	dBytes := bytes[nLength:]

	numerator := bytesToInt64(nBytes)
	denominator := bytesToInt64(dBytes)

	dNumerator := decimal.NewFromInt(numerator)
	dDenominator := decimal.NewFromInt(denominator)

	r := dNumerator.Div(dDenominator)

	return &r
}

const _byte7 byte = 0xFF >> 1
const _flag byte = 1 << 7

func decodeLength(bytes []byte) (int, []byte) {
	if len(bytes) == 0 {
		return 0, bytes
	}

	length := 0

	for len(bytes) > 0 {
		bt := bytes[0]
		bytes = bytes[1:]
		length = length << 7
		length = length + int(bt&_byte7)
		if (bt & _flag) != _flag {
			return length, bytes
		}
	}
	return length, bytes
}

func encodeLength(length int) []byte {
	var bytes []byte

	for length > 0 {
		var flag uint8
		if len(bytes) == 0 {
			flag = 0
		} else {
			flag = _flag
		}
		bt := byte((length & int(_byte7)) + int(flag))
		bytes = append([]byte{bt}, bytes...)
		length = length >> 7
	}
	if len(bytes) == 0 {
		return []byte{0}
	}
	return bytes
}

func bytesToInt64(bytes []byte) int64 {
	if len(bytes) == 0 {
		return 0
	}
	result := int64(0)
	//var result int64 = 0
	for _, v := range bytes {
		result = (result << 8) + int64(v)
	}
	result = result >> 1
	if (bytes[len(bytes)-1] & 1) == 1 {
		result = -result
	}
	return result

}

var bigZero = big.NewInt(0)
var bigOne = big.NewInt(1)
var fullBigByte = big.NewInt(0xFF)

func bigIntToBytes(value *big.Int) []byte {
	var lvalue big.Int
	lvalue.Abs(value)
	lvalue.Lsh(&lvalue, 1)
	if value.Cmp(bigZero) < 0 {
		lvalue.Add(&lvalue, bigOne)
	}

	nBytes := lvalue.BitLen()/8 + 1
	lst := make([]byte, nBytes, nBytes)

	for i := 0; i < nBytes; i++ {
		var bt big.Int
		bt.And(&lvalue, fullBigByte)
		lvalue.Rsh(&lvalue, 8)

		lst[nBytes-i-1] = byte(bt.Int64())
	}
	return lst
}
