package proto_utils

import (
	"bytes"
	"math/big"
	"testing"

	"github.com/shopspring/decimal"
)

func TestEncodePositiveDecimal(t *testing.T) {
	value := decimal.NewFromInt(7654323456765432)
	got := DecimalToWrapper(&value)
	wanted := []byte{7, 54, 99, 34, 29, 69, 219, 240, 2}

	same := bytes.Compare(got, wanted)
	if same != 0 {
		t.Errorf("got %v, wanted %v", got, wanted)
	}
}

func TestDecodePositiveDecimal(t *testing.T) {
	value := []byte{7, 54, 99, 34, 29, 69, 219, 240, 2}
	got := WrapperToDecimal(value)
	wanted := decimal.NewFromInt(7654323456765432)
	if wanted.Cmp(*got) != 0 {
		t.Errorf("got %v, wanted %v", got, wanted)
	}
}

func TestEncodeZeroDecimal(t *testing.T) {
	value := decimal.Decimal{}
	got := DecimalToWrapper(&value)
	wanted := []byte{1, 0, 2}

	same := bytes.Compare(got, wanted)
	if same != 0 {
		t.Errorf("got %v, wanted %v", got, wanted)
	}
}

func TestDecodeZeroDecimal(t *testing.T) {
	value := []byte{1, 0, 2}
	got := WrapperToRat(value)
	if got.Num().Int64() != 0 {
		t.Errorf("got %v, wanted 0", got)
	}
}

func TestEncodeNegativeDecimal(t *testing.T) {
	//value := big.NewRat(-54323456765, 1)
	value := decimal.NewFromInt(-54323456765)
	got := DecimalToWrapper(&value)
	wanted := []byte{5, 25, 75, 220, 85, 251, 2}

	same := bytes.Compare(got, wanted)
	if same != 0 {
		t.Errorf("got %v, wanted %v", got, wanted)
	}
}

func TestDecodeNegativeDecimal(t *testing.T) {
	value := []byte{5, 25, 75, 220, 85, 251, 2}
	got := WrapperToDecimal(value)
	wanted := decimal.NewFromInt(-54323456765)
	if wanted.Cmp(*got) != 0 {
		t.Errorf("got %v, wanted %v", got, wanted)
	}
}

func TestEncodeNilDecimal(t *testing.T) {
	got := DecimalToWrapper(nil)
	var wanted []byte

	same := bytes.Compare(got, wanted)
	if same != 0 {
		t.Errorf("got %v, wanted %v", got, wanted)
	}
}

func TestDecodeNilDecimal(t *testing.T) {
	var value []byte
	got := WrapperToDecimal(value)
	if got != nil {
		t.Errorf("got %v, wanted nil", got)
	}
}

func TestEncodeDecimalPi(t *testing.T) {

	value, err := decimal.NewFromString("3.1415926535897932")
	if err != nil {
		t.Error(err)
		return
	}
	got := DecimalToWrapper(&value)
	wanted := []byte{7, 55, 206, 79, 50, 187, 33, 166, 17, 195, 121, 55, 224, 128, 0}

	same := bytes.Compare(got, wanted)
	if same != 0 {
		t.Errorf("got %v, wanted %v", got, wanted)
	}
}

func TestDecodeDecimalPi(t *testing.T) {
	value := []byte{7, 55, 206, 79, 50, 187, 33, 166, 17, 195, 121, 55, 224, 128, 0}

	got := WrapperToDecimal(value)
	wanted, err := decimal.NewFromString("3.1415926535897932")
	if err != nil {
		t.Error(err)
		return
	}

	if wanted.Cmp(*got) != 0 {
		t.Errorf("got %v, wanted %v", got, wanted)
	}
}

func TestEncodeNegativeDecimalPi(t *testing.T) {
	value, err := decimal.NewFromString("-3.1415926535897932")
	if err != nil {
		t.Error(err)
		return
	}
	got := DecimalToWrapper(&value)
	wanted := []byte{7, 55, 206, 79, 50, 187, 33, 167, 17, 195, 121, 55, 224, 128, 0}

	same := bytes.Compare(got, wanted)
	if same != 0 {
		t.Errorf("got %v, wanted %v", got, wanted)
	}
}

func TestDecodeNegativeDecimalPi(t *testing.T) {
	value := []byte{7, 55, 206, 79, 50, 187, 33, 167, 17, 195, 121, 55, 224, 128, 0}

	got := WrapperToDecimal(value)

	wanted, err := decimal.NewFromString("-3.1415926535897932")
	if err != nil {
		t.Error(err)
		return
	}

	if wanted.Cmp(*got) != 0 {
		t.Errorf("got %v, wanted %v", got, wanted)
	}
}

func TestEncodePositiveRat(t *testing.T) {
	value := *big.NewRat(7654323456765432, 1)
	got := RatToWrapper(&value)
	wanted := []byte{7, 54, 99, 34, 29, 69, 219, 240, 2}

	same := bytes.Compare(got, wanted)
	if same != 0 {
		t.Errorf("got %v, wanted %v", got, wanted)
	}
}

func TestDecodePositiveRat(t *testing.T) {
	value := []byte{7, 54, 99, 34, 29, 69, 219, 240, 2}
	got := WrapperToRat(value)
	wanted := big.NewRat(7654323456765432, 1)
	if wanted.Cmp(got) != 0 {
		t.Errorf("got %v, wanted %v", got, wanted)
	}
}

func TestEncodeZeroRat(t *testing.T) {
	value := big.Rat{}
	got := RatToWrapper(&value)
	wanted := []byte{1, 0, 2}

	same := bytes.Compare(got, wanted)
	if same != 0 {
		t.Errorf("got %v, wanted %v", got, wanted)
	}
}

func TestDecodeZeroRat(t *testing.T) {
	value := []byte{1, 0, 2}
	got := WrapperToRat(value)
	if got.Num().Int64() != 0 {
		t.Errorf("got %v, wanted 0", got)
	}
}

func TestEncodeNegativeRat(t *testing.T) {
	value := big.NewRat(-54323456765, 1)
	got := RatToWrapper(value)
	wanted := []byte{5, 25, 75, 220, 85, 251, 2}

	same := bytes.Compare(got, wanted)
	if same != 0 {
		t.Errorf("got %v, wanted %v", got, wanted)
	}
}

func TestDecodeNegativeRat(t *testing.T) {
	value := []byte{5, 25, 75, 220, 85, 251, 2}
	got := WrapperToRat(value)
	wanted := big.NewRat(-54323456765, 1)
	if wanted.Cmp(got) != 0 {
		t.Errorf("got %v, wanted %v", got, wanted)
	}
}

func TestEncodeNilRat(t *testing.T) {
	got := RatToWrapper(nil)
	var wanted []byte

	same := bytes.Compare(got, wanted)
	if same != 0 {
		t.Errorf("got %v, wanted %v", got, wanted)
	}
}

func TestDecodeNilRat(t *testing.T) {
	var value []byte
	got := WrapperToRat(value)
	if got != nil {
		t.Errorf("got %v, wanted nil", got)
	}
}

func TestEncodeRatPi(t *testing.T) {

	value := &big.Rat{}
	err := value.UnmarshalText([]byte("3.1415926535897932"))
	if err != nil {
		t.Error(err)
		return
	}
	got := RatToWrapper(value)
	wanted := []byte{7, 55, 206, 79, 50, 187, 33, 166, 17, 195, 121, 55, 224, 128, 0}

	same := bytes.Compare(got, wanted)
	if same != 0 {
		t.Errorf("got %v, wanted %v", got, wanted)
	}
}

func TestDecodeRatPi(t *testing.T) {
	value := []byte{7, 55, 206, 79, 50, 187, 33, 166, 17, 195, 121, 55, 224, 128, 0}

	got := WrapperToRat(value)
	wanted := &big.Rat{}
	err := wanted.UnmarshalText([]byte("3.1415926535897932"))
	if err != nil {
		t.Error(err)
		return
	}

	if wanted.Cmp(got) != 0 {
		t.Errorf("got %v, wanted %v", got, wanted)
	}
}

func TestEncodeNegativeRatPi(t *testing.T) {
	value := &big.Rat{}
	err := value.UnmarshalText([]byte("-3.1415926535897932"))
	if err != nil {
		t.Error(err)
		return
	}
	got := RatToWrapper(value)
	wanted := []byte{7, 55, 206, 79, 50, 187, 33, 167, 17, 195, 121, 55, 224, 128, 0}

	same := bytes.Compare(got, wanted)
	if same != 0 {
		t.Errorf("got %v, wanted %v", got, wanted)
	}
}

func TestDecodeNegativeRatPi(t *testing.T) {
	value := []byte{7, 55, 206, 79, 50, 187, 33, 167, 17, 195, 121, 55, 224, 128, 0}

	got := WrapperToRat(value)
	wanted := &big.Rat{}
	err := wanted.UnmarshalText([]byte("-3.1415926535897932"))
	if err != nil {
		t.Error(err)
		return
	}

	if wanted.Cmp(got) != 0 {
		t.Errorf("got %v, wanted %v", got, wanted)
	}
}

func TestRatToString(t *testing.T) {
	value := big.NewRat(-123456, 100)
	got := RatToString(value)

	wanted := "-1234.56"
	if wanted != got {
		t.Errorf("got %s, wanted %s", got, wanted)
	}
}

func TestStringToRat(t *testing.T) {
	value := "-1234.56"
	got, err := StringToRat(value)
	if err != nil {
		t.Error(err)
		return
	}

	wanted := big.NewRat(-123456, 100)
	if wanted.Cmp(got) != 0 {
		t.Errorf("got %s, wanted %s", got, wanted)
	}
}

func TestDecimalToString(t *testing.T) {
	value, err := decimal.NewFromString("-1234.56")
	if err != nil {
		t.Error(err)
		return
	}
	got, err := DecimalToString(&value)
	if err != nil {
		t.Error(err)
		return
	}

	wanted := "-1234.56"
	if wanted != got {
		t.Errorf("got %s, wanted %s", got, wanted)
	}
}

func TestStringToDecimal(t *testing.T) {
	value := "-1234.56"
	got, err := StringToDecimal(value)
	if err != nil {
		t.Error(err)
		return
	}

	wanted, err := decimal.NewFromString("-1234.56")
	if err != nil {
		t.Error(err)
		return
	}
	if got.Cmp(wanted) != 0 {
		t.Errorf("got %s, wanted %s", got, wanted)
	}
}
